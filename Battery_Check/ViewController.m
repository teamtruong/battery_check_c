//
//  ViewController.m
//  Battery_Check
//
//  Created by Thien Truong on 2/7/21.
//

#import "ViewController.h"

@implementation ViewController

@synthesize buttonPressProduct = _buttonPressProduct;
@synthesize buttonPressSerial = _buttonPressSerial;
@synthesize buttonPressMac = _buttonPressMac;
@synthesize buttonPressBattery = _buttonPressBattery;

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
}


- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}

- (IBAction) simpleButtonPressed:(id) sender {

    io_iterator_t hidObjectIterator;
    IOReturn ioReturnValue = IOServiceGetMatchingServices(kIOMasterPortDefault, IOServiceMatching("AppleDeviceManagementHIDEventService"), &hidObjectIterator);
        if (ioReturnValue != kIOReturnSuccess)
        {
            self.buttonPressProduct.stringValue = @"Device Not Found";
            self.buttonPressSerial.stringValue = @"N/A";
            self.buttonPressMac.stringValue = @"N/A";
            self.buttonPressBattery.stringValue = @"N/A";
            // print error
        }
        else {
            
            io_service_t service;
                while ((service = IOIteratorNext(hidObjectIterator)) != IO_OBJECT_NULL) {
                    CFStringRef product_key = CFSTR("Product");
                    CFStringRef battery_key = CFSTR("BatteryPercent");
                    CFStringRef mac_key = CFSTR("DeviceAddress");
                    CFStringRef serial_key = CFSTR("SerialNumber");
                    CFStringRef serial_value = IORegistryEntryCreateCFProperty(service, serial_key, kCFAllocatorDefault, 0);
                    CFStringRef product_value = IORegistryEntryCreateCFProperty(service, product_key, kCFAllocatorDefault, 0);
                    CFStringRef battery_value = IORegistryEntryCreateCFProperty(service, battery_key, kCFAllocatorDefault, 0);
                    CFStringRef mac_value = IORegistryEntryCreateCFProperty(service, mac_key, kCFAllocatorDefault, 0);


                    if (battery_value != NULL) {
                        
                        // found battery vlaue!
//                        CFShow(product_value);
//                        CFShow(serial_value);
//                        CFShow(battery_value);
//                        CFShow(mac_value);
                        self.buttonPressProduct.stringValue = CFBridgingRelease(product_value);
                        self.buttonPressSerial.stringValue = CFBridgingRelease(serial_value);
                        self.buttonPressMac.stringValue = CFBridgingRelease(mac_value);
                        self.buttonPressBattery.stringValue = CFBridgingRelease(battery_value);
                    }
                    else {
                            self.buttonPressProduct.stringValue = @"Device Not Found";
                            self.buttonPressSerial.stringValue = @"N/A";
                            self.buttonPressMac.stringValue = @"N/A";
                            self.buttonPressBattery.stringValue = @"N/A";
                            // print error
                    }
                    IOObjectRelease(service);
                }
        }

    IOObjectRelease(hidObjectIterator);
}
@end
