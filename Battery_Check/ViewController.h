//
//  ViewController.h
//  Battery_Check
//
//  Created by Thien Truong on 2/7/21.
//

#import <Cocoa/Cocoa.h>

@interface ViewController : NSViewController {
    
    int count;
}

@property (nonatomic, retain) IBOutlet NSTextField *buttonPressProduct;
@property (nonatomic, retain) IBOutlet NSTextField *buttonPressSerial;
@property (nonatomic, retain) IBOutlet NSTextField *buttonPressMac;
@property (nonatomic, retain) IBOutlet NSTextField *buttonPressBattery;


-(IBAction)simpleButtonPressed:(id)sender;
@end

